import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-authwrap',
    version='3.0.4',
    packages=['authwrap'],
    include_package_data=True,
    license='MIT License',  # example license
    description='A django user management app.',
    long_description=README,
    url='https://bitbucket.org/dukeofweatherby/django-authwrap',
    install_requires=[
        "django-appconf>=0.6",
        "pytz>=2013.9",
        "django-bootstrap-form>=3.0"
    ],
    author='Jamie Weatherby',
    author_email='jamie.weatherby@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)