#!/usr/bin/env python
import os
import sys
import conf
from django.conf import settings

if __name__ == "__main__":
    settings.configure(**conf.globals)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
