import os
import sys
import django
from django.conf import settings
import conf

settings.configure(**conf.globals)
if hasattr(django, "setup"):
    django.setup()

from django_nose import NoseTestSuiteRunner

test_runner = NoseTestSuiteRunner(verbosity=1)
failures = test_runner.run_tests(["authwrap"])
if failures:
    sys.exit(failures)