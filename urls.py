from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
import example.views

urlpatterns = patterns(
    "",
    url(r"^$", example.views.home, name='home'),
    url(r"^account/", include('authwrap.urls', namespace='authwrap')),
)
