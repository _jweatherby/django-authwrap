import os

from django.conf import settings
PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__)))
globals = {
    'DEBUG': True,
    'TEMPLATE_DEBUG': True,
    'USE_TZ': True,
    'DATABASES': {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            'NAME': os.path.join(PROJECT_ROOT, 'authwrap.sqlite3')
        }
    },
    'ROOT_URLCONF': "urls",
    'INSTALLED_APPS': [
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sites",
        "django.contrib.messages",
        'django.contrib.sessions',
        "bootstrapform",
        "authwrap",
        "example"
    ],
    'MIDDLEWARE_CLASSES': [
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    ],
    'SITE_ID': 1,
    'TEMPLATE_DIRS': [
        os.path.join(PROJECT_ROOT, "authwrap", "templates", "authwrap"),
        os.path.join(PROJECT_ROOT, "authwrap", "templates", "emails"),
    ],
    'NOSE_ARGS': ['--nologcapture', ],
    'EMAIL_BACKEND': 'django.core.mail.backends.console.EmailBackend',
    'AUTH_USER_MODEL': 'authwrap.Account',
    'HOSTNAME': 'http://127.0.0.1:8080',
    'FROM_EMAIL': 'fromemail@example.com',
    'SUPPORT_EMAIL': 'supportemail@example.com',
    'LOGGED_IN_REDIRECT_URL': 'authwrap:profile',
    'EMAIL_TEMPLATES': {
        'TEST2': {
            'text': 'emails/test2.txt',
            'template': 'emails/test2.html',
            'subject': 'This is a test of the email templates overrides'
        },
    }
}