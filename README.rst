========
Authwrap
========

Authwrap is a custom user management system for django.

Quick start
-----------

1. Add "accounts" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'authwrap',
    )

2. Include the polls URLconf in your project urls.py like this::

    url(r'^accounts/', include('authwrap.urls', namespace='authwrap')),

**Note: a url named 'home' must be present.*

3. In the settings, add::

    AUTH_USER_MODEL = 'accounts.Account'
    HOSTNAME = 'http://127.0.0.1:8000'
    FROM_EMAIL = 'from_email@example.com'
    SUPPORT_EMAIL = 'support_email@example.com'
    LOGGED_IN_REDIRECT_URL = 'home'

5. Run `python manage.py migrate` to create the accounts models.

6. Start the development server and visit http://127.0.0.1:8000/accounts/register
   to create an account.


Usage
-----

This app creates a series of views and associated default templates
that are automatically included into the project. This has
been tested with pinax-project-zero and works to specification.

The available views include:

- login
- register
- send_activation
- forgot_password
- change_password
- change_email
- deactivate
- activate
- activate_email
- reset_password
- logout

Their respective templates can be found at "accounts/\*". They inherit from "site_base.html" in the
The templates for the email can be found in "emails/\*" and inherit from "emails/_base.html"
All these templates can be overwritten using the corresponding directories.

Of those views, the following accept ajax calls and respond with an associative array in json format:

- login
- register
- send_activation
- forgot_password
- change_password
- change_email

The following views require a token which can be accessed from an email template:

- activate
- activate_email
- reset_password

The remaining views log a user out and return to a url named 'home':

- logout
- deactivate

Emails
------

To add additional email templates to the project, add::

    EMAIL_TEMPLATES = {
        'NEW_EMAIL_TYPE' : {
            'text': 'emails/text_content_template.txt',
            'template': 'emails/html_rendered_template.html',
            'subject': 'Subject for said email'
        }
    }

The email can be sent by calling::

    user = Account() # user to send the email to
    user.send_to_account('NEW_EMAIL_TYPE', <Dict of params to add to email>)
