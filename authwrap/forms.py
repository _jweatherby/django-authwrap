from django import forms
from django.contrib.auth.hashers import check_password
from django.contrib.auth import get_user_model
# from authwrap.models import Account


class LoginForm(forms.Form):

    email = forms.EmailField(widget=forms.widgets.TextInput)
    password = forms.CharField(widget=forms.widgets.PasswordInput)
    account = get_user_model()

    def is_valid(self):

        valid = super(LoginForm, self).is_valid()
        if not valid:
            return valid

        password = self.cleaned_data['password']
        email = self.cleaned_data['email']

        try:
            acct = self.account.objects.get(email=email)

            if check_password(password, acct.password) is False:
                self._errors['password'] = \
                    ['That email password combination could not be found']
                return False

            if acct.is_active is False:
                self._errors['email'] = ['That account is not active.']
                return False

        except self.account.DoesNotExist:
            self._errors['email'] = ['That email does not exist']
            return False

        return True

    class Meta:
        fields = ['email', 'password']


class RegistrationForm(forms.ModelForm):

    email = forms.EmailField(required=True)
    password1 = forms.CharField(
        label="Password",
        widget=forms.PasswordInput(attrs={
            'autocomplete': 'off'
        }),
    )
    password2 = forms.CharField(
        label="Confirm your password",
        widget=forms.PasswordInput(attrs={
            'autocomplete': 'off'
        }),
    )
    account = get_user_model()

    def is_valid(self):

        valid = super(RegistrationForm, self).is_valid()
        if not valid:
            return valid

        email = self.cleaned_data['email']

        if self.account.objects.filter(email=email).exists():
            self._errors['email'] = ['That email already exists']
            return False

        p1 = self.cleaned_data['password1']
        p2 = self.cleaned_data['password2']

        p1_p2_match = self.account.check_passwords_match(p1, p2)
        if p1_p2_match:
            self._errors = p1_p2_match
            return False

        return True

    class Meta:
        model = get_user_model()
        exclude = ('last_login', 'password', 'is_admin', 'is_active')


class ChangeEmailForm(forms.Form):

    email = forms.EmailField(label="New Email Address",
                             required=True)
    account = get_user_model()

    def is_valid(self):
        valid = super(ChangeEmailForm, self).is_valid()
        if not valid:
            return valid

        try:
            self.account.objects.get(email=self.cleaned_data['email'])
            self._errors['email'] = ['That email already exists']
            return False
        except self.account.DoesNotExist:
            return True

    class Meta:
        fields = ('email',)


class ChangePasswordForm(forms.Form):

    password1 = forms.CharField(
        label="Enter new password",
        widget=forms.PasswordInput(attrs={
            'autocomplete': 'off'
        }),
        required=True,
    )
    password2 = forms.CharField(
        label="Confirm new password",
        widget=forms.PasswordInput(attrs={
            'autocomplete': 'off'
        }),
        required=True,
    )
    account = get_user_model()

    def is_valid(self):
        valid = super(ChangePasswordForm, self).is_valid()
        if not valid:
            return valid

        p1 = self.cleaned_data['password1']
        p2 = self.cleaned_data['password2']

        p1_p2_match = self.account.check_passwords_match(p1, p2)
        if p1_p2_match:
            self._errors = p1_p2_match
            return False

        return True

    class Meta:
        fields = ('password1', 'password2')


class ActivationForm(forms.Form):

    email = forms.EmailField(required=True)
    account = get_user_model()

    class Meta:
        model = get_user_model()
        fields = ('email',)

    def is_valid(self):

        valid = super(ActivationForm, self).is_valid()
        if not valid:
            return valid

        try:
            acct = self.account.objects.get(email=self.cleaned_data['email'])
        except self.account.DoesNotExist:
            self._errors['email'] = ['That email does not exist']
            return False

        if acct.is_active is True:
            self._errors['email'] = ['That account is already active']
            return False

        return True


class ForgotPasswordForm(forms.Form):

    email = forms.EmailField(required=True)
    account = get_user_model()

    class Meta:
        fields = ('email',)

    def is_valid(self):

        valid = super(ForgotPasswordForm, self).is_valid()
        if not valid:
            return valid

        try:
            acct = self.account.objects.get(email=self.cleaned_data['email'])
        except self.account.DoesNotExist:
            self._errors['email'] = ['That email does not exist']
            return False

        return True