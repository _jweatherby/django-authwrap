from authwrap.tests.basetest import BaseTest
from authwrap.forms import (RegistrationForm,
                            ActivationForm, 
                            ForgotPasswordForm,
                            ChangePasswordForm,
                            ChangeEmailForm)


class AccountsRegistrationFormTest(BaseTest):
    """
        Testing guest forms - login + register
    """
    def test_registration_form_with_no_variables(self):
        form = RegistrationForm({})
        self.assertIn('required', form.errors['email'][0])
        self.assertIn('required', form.errors['password1'][0])
        self.assertIn('required', form.errors['password2'][0])
        self.assertEqual(form.is_valid(), False)

    def test_registration_form_with_incorrect_form_data(self):
        # invalid email format and passwords don't match
        form_data = {
            'email': 'user',
            'password1': 'password',
            'password2': 'password1',
        }
        form = RegistrationForm(form_data)
        self.assertEqual(form.is_valid(), False)
        self.assertIn('valid', form.errors['email'][0])
        form_data['email'] = 'user@example.com'
        form = RegistrationForm(form_data)
        self.assertEqual(form.is_valid(), False)
        self.assertIn('match', form.errors['password2'][0])
        form_data['password2'] = form_data['password1']
        form = RegistrationForm(form_data)
        self.dump(form.errors)
        self.assertEqual(form.is_valid(), True)

    def test_successful_registration_form(self):
        form_data = {
            'email': 'user@example.com',
            'password1': 'password',
            'password2': 'password',
        }
        form = RegistrationForm(form_data)
        self.dump(form.errors)
        self.assertEqual(form.is_valid(), True)


class AccountsActivationFormTest(BaseTest):

    def test_send_activation_form_with_invalid_emails(self):
        tests = (
            {'email': '', 'error': 'required'},
            {'email': 'user', 'error': 'valid'},
            {'email': 'user@doesnotexist.com', 'error': 'exist'}
        )
        for test in tests:
            form = ActivationForm({'email': test['email']})
            self.assertEqual(form.is_valid(), False)
            self.assertIn(test['error'], form.errors['email'][0])

    def test_send_activation_with_active_account_fails(self):
        form = ActivationForm({'email': self.acct.email})
        self.assertEqual(form.is_valid(), False)
        self.assertIn('already active', form.errors['email'][0])

    def test_send_activation_form_with_valid_input(self):
        self.acct.is_active = False
        self.acct.save()
        form = ActivationForm({'email': self.acct})
        self.assertEqual(form.is_valid(), True)


class AccountsForgotPasswordFormTest(BaseTest):

    def test_send_activation_form_with_invalid_emails(self):
        tests = (
            {'email': '', 'error': 'required'},
            {'email': 'user', 'error': 'valid'},
            {'email': 'user@doesnotexist.com', 'error': 'exist'}
        )
        for test in tests:
            form = ForgotPasswordForm({'email': test['email']})
            self.assertEqual(form.is_valid(), False)
            self.assertIn(test['error'], form.errors['email'][0])

    def test_forgot_password_with_inactive_account_succeeds(self):
        self.acct.is_active = False
        self.acct.save()
        form = ForgotPasswordForm({'email': self.acct.email})
        self.assertEqual(form.is_valid(), True)
        
    def test_send_activation_form_with_valid_input(self):
        form = ForgotPasswordForm({'email': self.acct})
        self.assertEqual(form.is_valid(), True)


class AccountsChangePasswordForm(BaseTest):

    def test_change_password_with_no_input(self):
        form_data = {
            'password1': '',
            'password2': ''
        }
        form = ChangePasswordForm(form_data)
        self.assertEqual(form.is_valid(), False)
        self.assertIn('required', form.errors['password1'][0])
        self.assertIn('required', form.errors['password2'][0])

    def test_change_password_with_bad_input(self):
        form_data = {
            'password1': 'password',
            'password2': 'password1'
        }
        form = ChangePasswordForm(form_data)
        self.assertEqual(form.is_valid(), False)
        self.assertIn('match', form.errors['password2'][0])

    def test_change_password_with_valid_input(self):
        form_data = {
            'password1': 'password',
            'password2': 'password'
        }
        form = ChangePasswordForm(form_data)
        self.assertEqual(form.is_valid(), True)
        self.dump(form.errors)


class AccountsChangeEmailFormTest(BaseTest):

    def test_change_email_form_with_invalid_emails(self):
        tests = (
            {'email': '', 'error': 'required'},
            {'email': 'user', 'error': 'valid'},
            {'email': self.acct.email, 'error': 'already exists'}
        )
        for test in tests:
            form = ChangeEmailForm({'email': test['email']})
            self.assertEqual(form.is_valid(), False)
            self.assertIn(test['error'], form.errors['email'][0])

    def test_change_email_form_with_valid_input(self):
        form = ChangeEmailForm({'email': 'new.email@example.com'})
        self.assertEqual(form.is_valid(), True)
