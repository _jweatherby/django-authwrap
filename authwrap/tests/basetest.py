import pprint
import json
from django.core.urlresolvers import reverse
from django.test import TestCase, Client
from django.test.client import RequestFactory
from django.contrib.auth.hashers import make_password

from authwrap.models import Account


class BaseTest(TestCase):
    def create_default_user(self):
        params = {
            "password": "password",
            "email": "admin@test.com",
        }
        self.acct = Account.objects\
            .create_user(email=params['email'],
                         password=params['password'])
        self.acct.is_active = True
        self.acct.save()

    def setUp(self):
        """
           initiate the browser, the client and the requestfactory
        """
        # self.browser = webdriver.Firefox()
        self.client = Client()
        self.request = RequestFactory()
        self.create_default_user()

    def tearDown(self):
        """
           if using selenium, quit the browser
        """
        # self.browser.quit()

    def dump(self, content):
        pprint.pprint(content)

    def ajax_request(self, named_url, form_data):
        response = self.client.post(
            reverse(named_url),
            form_data,
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        payload = json.loads(response.content)
        self.dump(reverse(named_url))
        self.dump(payload)
        return payload

    def post_request(self, named_url, form_data):
        response = self.client.post(
            reverse(named_url),
            form_data,
        )
        self.dump(reverse(named_url))
        self.dump(response)
        return response

    def login(self):
        self.authenticated = self.client.login(username=self.acct.email,
                                               password='password')

    # def test_title_in_browser(self):
        # """ if using selenium """
        # self.browser.get('http://127.0.0.1:8000')
        #  self.assertIn('Ultimate Club Manager', self.browser.title)
