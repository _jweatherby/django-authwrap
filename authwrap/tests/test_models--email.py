from .basetest import BaseTest

from authwrap.models import Mail


class TestMailModel(BaseTest):

    def send_test_email(self):
        email = Mail().send_to_account(self.acct, "TEST", {})
        self.assertIn('123', email.message)
        self.assertEqual(len(Mail.get_outbox()), 1)

    def send_test2_email(self):
        email = Mail().send_to_account(self.acct, "TEST2", {})
        self.assertIn('456', email.message)
        self.assertEqual(len(Mail.get_outbox()), 1)

