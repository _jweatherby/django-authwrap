from django.core.urlresolvers import reverse, resolve
from authwrap.tests.basetest import BaseTest

from authwrap import views
from authwrap.models import Account, Token


class GenericViewsTest(BaseTest):

    def test_generic_views_exist(self):
        account_views = {
            'authwrap:login': views.login,
            'authwrap:register': views.register,
            'authwrap:send-activation': views.send_activation,
            'authwrap:deactivate': views.deactivate,
            'authwrap:forgot-password': views.forgot_password,
            'authwrap:change-password': views.change_password,
            'authwrap:change-email': views.change_email,
            'authwrap:logout': views.logout
        }
        for url, view in account_views.iteritems():
            found = resolve(reverse(url))
            self.assertEqual(found.func, view)

    def test_views_with_tokens_exist(self):
        account_views = {
            'authwrap:activate': views.activate,
            'authwrap:reset-password': views.reset_password,
            'authwrap:activate-email': views.activate_email,
        }
        for url, view in account_views.iteritems():
            found = resolve(reverse(url, kwargs={'token': '123456'}))
            self.assertEqual(found.func, view)


class LoginViewTest(BaseTest):

    def test_login_view_with_get(self):
        req = self.client.get(reverse('authwrap:login'))
        self.assertIn('form', req.content)

    def test_login_view_get_and_next(self):
        url = "%s?next=%s" % \
              (reverse('authwrap:login'),
               reverse('authwrap:change-email'))
        req = self.client.get(url)
        self.assertIn('change-email', req.content)

    def test_empty_login_with_ajax(self):
        post_data = {
            'email': '',
            'password': ''
        }
        req = self.ajax_request('authwrap:login', post_data)
        self.assertEqual(False, req['success'])
        self.assertEqual(2, len(req['errors']))

    def test_bad_login_with_ajax(self):
        post_data = {
            'email': self.acct.email,
            'password': 'password1'
        }
        req = self.ajax_request('authwrap:login', post_data)
        self.assertEqual(False, req['success'])
        self.assertEqual(1, len(req['errors']))

    def test_good_login_with_ajax(self):
        post_data = {
            'email': self.acct.email,
            'password': 'password'
        }
        req = self.ajax_request('authwrap:login', post_data)
        self.assertEqual(True, req['success'])

    def test_bad_login_with_post(self):
        post_data = {
            'email': self.acct.email,
            'password': 'password1'
        }
        response = self.post_request('authwrap:login', post_data)
        self.assertIn('form', response.content)

    def test_good_login_with_post(self):
        post_data = {
            'email': self.acct.email,
            'password': 'password',
            'next': ''
        }
        response = self.post_request('authwrap:login', post_data)
        self.assertEqual(response.status_code, 302)

    def test_good_login_with_post_and_next(self):
        post_data = {
            'email': self.acct.email,
            'password': 'password',
            'next': reverse('authwrap:change-email')
        }
        response = self.post_request('authwrap:login', post_data)
        self.assertRedirects(response, reverse('authwrap:change-email'))


class RegisterViewTests(BaseTest):

    def test_register_view_with_get(self):
        req = self.client.get(reverse('authwrap:register'))
        self.assertIn('form', req.content)

    def test_empty_register_with_ajax(self):
        post_data = {
            'email': '',
            'password1': '',
            'password2': '',
        }
        req = self.ajax_request('authwrap:register', post_data)
        self.assertEqual(False, req['success'])
        self.assertEqual(3, len(req['errors']))

    def test_bad_register_with_ajax(self):

        # error is invalid email address
        post_data = {
            'email': 'user',
            'password1': 'password',
            'password2': 'password1',
        }
        req = self.ajax_request('authwrap:register', post_data)
        self.assertEqual(False, req['success'])
        self.assertEqual(1, len(req['errors']))
        self.assertIn('valid', req['errors']['email'][0])

        # error is existing email address
        post_data['email'] = self.acct.email
        req = self.ajax_request('authwrap:register', post_data)
        self.assertEqual(False, req['success'])
        self.assertEqual(1, len(req['errors']))
        self.assertIn('exists', req['errors']['email'][0])

        # test passwords do not match
        post_data['email'] = 'user@doesnotexist.com'
        req = self.ajax_request('authwrap:register', post_data)
        self.assertEqual(False, req['success'])
        self.assertEqual(1, len(req['errors']))
        self.assertIn('match', req['errors']['password2'][0])

        # test password is too short
        post_data['password1'] = 'pass'
        req = self.ajax_request('authwrap:register', post_data)
        self.assertEqual(False, req['success'])
        self.assertEqual(1, len(req['errors']))
        self.assertIn('at least', req['errors']['password1'][0])

    def test_good_register_with_ajax(self):
        post_data = {
            'email': 'user@doesnotcurrentlyexist.com',
            'password1': 'password',
            'password2': 'password',
        }
        req = self.ajax_request('authwrap:register', post_data)
        self.assertEqual(True, req['success'])

    def test_good_register_with_post(self):
        post_data = {
            'email': 'user@doesnotcurrentlyexist.com',
            'password1': 'password',
            'password2': 'password',
        }
        req = self.post_request('authwrap:register', post_data)
        self.assertIn('successfully registered', req.content)

    def test_bad_register_with_post(self):
        post_data = {
            'email': '',
            'password1': '',
            'password2': '',
        }
        req = self.post_request('authwrap:register', post_data)
        self.assertIn('form', req.content)
        self.assertNotIn('successfully registered', req.content)


class SendActivationViewTest(BaseTest):

    def test_send_activation_with_get(self):
        req = self.client.get(reverse('authwrap:send-activation'))
        self.assertIn('form', req.content)

    def test_bad_send_activation_with_ajax(self):
        tests = (
            {'email': '', 'error': 'required'},
            {'email': 'user', 'error': 'valid'},
            {'email': 'user@doesnotexist.com', 'error': 'exist'}
        )
        for test in tests:
            response = self.ajax_request('authwrap:send-activation', test)
            self.assertEqual(response['success'], False)
            self.assertIn(test['error'], response['errors']['email'][0])

    def test_bad_already_active_send_activation_with_ajax(self):
        test_data = {'email': self.acct.email}
        response = self.ajax_request('authwrap:send-activation', test_data)
        self.assertEqual(response['success'], False)
        self.assertIn('already active', response['errors']['email'][0])

    def test_good_send_activation_with_ajax(self):
        self.acct.is_active = False
        self.acct.save()
        form_data = {'email': self.acct.email}
        response = self.ajax_request('authwrap:send-activation', form_data)
        self.assertEqual(response['success'], True)

    def test_good_send_activation_with_post(self):
        self.acct.is_active = False
        self.acct.save()
        post_data = {'email': self.acct.email}
        response = self.post_request('authwrap:send-activation', post_data)
        self.assertIn('email has been sent', response.content)

    def test_bad_send_activation_with_post(self):
        post_data = {'email': 'user@doesnotexist.com'}
        response = self.post_request('authwrap:send-activation', post_data)
        self.assertIn('form', response.content)
        self.assertNotIn('email has been sent', response.content)


class ForgotPasswordViewTest(BaseTest):

    def test_forgot_password_with_get(self):
        req = self.client.get(reverse('authwrap:forgot-password'))
        self.assertIn('form', req.content)

    def test_bad_forgot_password_with_ajax(self):
        post_data = {'email': 'user@doesnotexist.com'}
        req = self.ajax_request('authwrap:forgot-password', post_data)
        self.assertFalse(req['success'])
        self.assertEqual(1, len(req['errors']))

    def test_good_forgot_password_with_ajax(self):
        post_data = {'email': self.acct.email}
        req = self.ajax_request('authwrap:forgot-password', post_data)
        self.assertTrue(req['success'])

    def test_bad_forgot_password_with_post(self):
        post_data = {'email': 'user@doesnotexist.com'}
        req = self.post_request('authwrap:forgot-password', post_data)
        self.assertNotIn('email has been sent', req.content)
        self.assertIn('form', req.content)

    def test_good_forgot_password_with_post(self):
        post_data = {'email': self.acct.email}
        req = self.post_request('authwrap:forgot-password', post_data)
        self.assertIn('email has been sent', req.content)
        self.assertIn('form', req.content)


class ChangePasswordViewTest(BaseTest):

    def test_change_password_with_get(self):
        self.login()
        req = self.client.get(reverse('authwrap:change-password'))
        self.assertIn('form', req.content)

    def test_bad_change_password_with_ajax(self):
        self.login()
        post_data = {'password1': 'password', 'password2': 'password1'}
        req = self.ajax_request('authwrap:change-password', post_data)
        self.assertFalse(req['success'])
        self.assertEqual(1, len(req['errors']))

    def test_good_change_password_with_ajax(self):
        self.login()
        p_original = self.acct.password
        post_data = {'password1': 'password', 'password2': 'password'}
        req = self.ajax_request('authwrap:change-password', post_data)
        self.assertTrue(req['success'])
        p_new = Account.objects.get(pk=self.acct.pk).password
        self.assertNotEqual(p_original, p_new)

    def test_bad_change_password_with_post(self):
        self.login()
        post_data = {'password1': 'password', 'password2': 'password1'}
        req = self.post_request('authwrap:change-password', post_data)
        self.assertNotIn('email has been sent', req.content)
        self.assertIn('form', req.content)

    def test_good_change_password_with_post(self):
        self.login()
        post_data = {'password1': 'password', 'password2': 'password'}
        req = self.post_request('authwrap:change-password', post_data)
        self.assertIn('email has been sent', req.content)
        self.assertIn('form', req.content)


class ChangeEmailViewTest(BaseTest):

    def test_change_email_with_get(self):
        self.login()
        req = self.client.get(reverse('authwrap:change-email'))
        self.assertIn('form', req.content)

    def test_bad_change_email_with_ajax(self):
        self.login()
        post_data = {'email': ''}
        req = self.ajax_request('authwrap:change-email', post_data)
        self.assertFalse(req['success'])
        self.assertEqual(1, len(req['errors']))

    def test_good_change_email_with_ajax(self):
        self.login()
        post_data = {'email': 'user@doesnotexist.com'}
        req = self.ajax_request('authwrap:change-email', post_data)
        self.assertTrue(req['success'])

    def test_bad_change_email_with_post(self):
        self.login()
        post_data = {'email': ''}
        req = self.post_request('authwrap:change-email', post_data)
        self.assertNotIn('email has been sent', req.content)
        self.assertIn('form', req.content)

    def test_good_change_email_with_post(self):
        self.login()
        post_data = {'email': 'user@doesnotexist.com'}
        req = self.post_request('authwrap:change-email', post_data)
        self.assertIn('email has been sent', req.content)
        self.assertIn('form', req.content)


class DeactivateViewTest(BaseTest):

    def test_deactivate_with_get(self):
        self.login()
        req = self.client.get(reverse('authwrap:deactivate'))
        self.assertIn('form', req.content)

    def test_deactivate_with_post(self):
        self.login()
        redirect = "%s?msg=DEACTIVATED" % reverse('home')
        req = self.post_request('authwrap:deactivate', {'deactivate': True})
        self.assertRedirects(req, redirect)


class ActivateViewTest(BaseTest):

    def test_bad_activate_with_get(self):
        kwargs = {'token': '123'}
        activate_url = reverse('authwrap:activate', kwargs=kwargs)
        response = self.client.get(activate_url)
        self.assertEqual(response.status_code, 404)

    def test_good_activate_with_get(self):
        # simple, valid registration
        self.acct.is_active = False
        self.acct.save()
        token = Token.create(self.acct, 'ACTIVATION', 16)
        kwargs = {'token': token.token}
        activate_url = reverse('authwrap:activate', kwargs=kwargs)
        response = self.client.get(activate_url)
        self.assertEqual(response.status_code, 302)


class ActivateEmailViewTest(BaseTest):

    def test_bad_activate_email_with_get(self):
        kwargs = {'token': '123'}
        activate_url = reverse('authwrap:activate-email', kwargs=kwargs)
        response = self.client.get(activate_url)
        self.assertEqual(response.status_code, 404)

    def test_good_activate_email_with_get(self):
        # simple, valid registration
        self.acct.is_active = False
        self.acct.save()
        token = Token.create(self.acct, 'CHANGE_EMAIL', 16)
        kwargs = {'token': token.token}
        activate_url = reverse('authwrap:activate-email', kwargs=kwargs)
        response = self.client.get(activate_url)
        redirect = "%s?msg=EMAIL_ACTIVATED" % reverse('home')
        self.assertEqual(response.status_code, 302)


class ResetPasswordViewTest(BaseTest):

    def test_bad_reset_password_with_get(self):
        kwargs = {'token': '123'}
        activate_url = reverse('authwrap:reset-password', kwargs=kwargs)
        response = self.client.get(activate_url)
        self.assertEqual(response.status_code, 404)

    def test_good_reset_password_with_get(self):
        # simple, valid registration
        self.acct.is_active = False
        self.acct.save()
        token = Token.create(self.acct, 'FORGOT_PASSWORD', 16)
        kwargs = {'token': token.token}
        reset_url = reverse('authwrap:reset-password', kwargs=kwargs)
        response = self.client.get(reset_url)
        self.assertRedirects(response, reverse('authwrap:change-password'))
        self.acct = Account.objects.get(pk=self.acct.pk)
        self.assertEqual(self.acct.is_active, True)


class ViewEmailText(BaseTest):

    def test_valid_access(self):
        mail = self.acct.send_email_with_token("FORGOT_PASSWORD")
        url = reverse('authwrap:view-email',
                      kwargs={'access': mail.access})
        response = self.client.get(url)
        self.assertIn("Forgot your password?", response.content)
        self.assertIn("view-email", response.content)
        self.assertIn("supportemail", response.content)
        self.dump(response.content)
        self.assertEqual(response.status_code, 200)
