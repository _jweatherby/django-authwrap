from authwrap.tests.basetest import BaseTest
from authwrap.models import Token


class TokensModelsTests(BaseTest):

    def test_generate_token(self):
        token = Token().generate()
        self.assertEqual(len(token), 10)
        token = Token().generate(length=8)
        self.assertEqual(len(token), 8)

    def test_create_token_with_invalid_input(self):
        token = Token.create(self.acct, 'DOES_NOT_EXIST')
        self.assertEqual(token, None)

    def test_create_token_with_valid_input(self):
        token = Token.create(self.acct, 'ACTIVATION')
        self.assertEqual(token.pk, 1)

    def test_get_token_by_type(self):
        t = Token.create(self.acct, 'ACTIVATION')
        token = Token.get_token_by_type(t.token, 'FORGOT_PASSWORD')
        self.assertEqual(token, None)
        token = Token.get_token_by_type(t.token, 'ACTIVATION')
        self.assertEqual(isinstance(token, Token), True)
