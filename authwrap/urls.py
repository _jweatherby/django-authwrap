from django.conf.urls import patterns, url
from django.contrib import admin

from . import views

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', views.profile, name='profile'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^register/$', views.register, name='register'),
    url(r'^deactivate/$', views.deactivate, name='deactivate'),
    url(r'^send-activation/$', views.send_activation, name='send-activation'),
    url(r'^forgot-password/$', views.forgot_password, name='forgot-password'),
    url(r'^change-password/$', views.change_password, name='change-password'),
    url(r'^change-email/$', views.change_email, name='change-email'),

    url(r'^reset-password/(?P<token>[\w\d]+)/$', views.reset_password, name='reset-password'),
    url(r'^activate/(?P<token>[\w\d]+)/$', views.activate, name='activate'),
    url(r'^activate-email/(?P<token>[\w\d]+)/$', views.activate_email, name='activate-email'),

    url(r'^view-email/(?P<access>[\w\d]+)/$', views.view_email, name='view-email'),
)
