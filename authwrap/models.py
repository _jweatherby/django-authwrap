import time
import hashlib
import random
from django.db import models
from django.core import mail
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from django.template import Context
from django.template.loader import get_template
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser


class AccountManager(BaseUserManager):
    def create_user(self, email, password=None):
        user = self.model(email=email)
        user.assign_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None):
        user = self.create_user(email, password=password)
        user.is_admin = True
        user.is_active = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser):

    USERNAME_FIELD = 'email'

    email = models.EmailField(max_length=254, unique=True, db_index=True)

    joined = models.DateTimeField(auto_now_add=True)

    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    objects = AccountManager()

    def __str__(self):
        return self.email

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        # Handle whether the user has a specific permission?"
        return True

    def has_module_perms(self, app_label):
        # Handle whether the user has permissions to view the app `app_label`?"
        return True

    def get_short_name(self):
        return self.email

    def get_full_name(self):
        return self.email

    @property
    def is_staff(self):
        # Handle whether the user is a member of staff?"
        return self.is_admin

    def assign_password(self, raw_password, confirm_update=None):
        self.password = make_password(raw_password)
        self.save()
        params = {'email': self.email}
        if confirm_update:
            self.send_email('CONFIRM_PASSWORD_CHANGE', params)

    @staticmethod
    def check_passwords_match(p1, p2):
        if len(p1) < 6:
            return {'password1': ['Must be at least 6 characters']}
        if p1 != p2:
            return {'password2': ['The passwords do not match']}
        return None

    def send_email(self, form_type, params=None):
        email = Mail()
        email.send_to_account(self, form_type, params)
        return email

    def create_token(self, token_type, data=''):
        token_length = Token.TOKEN_TYPES[token_type]['length']
        try:
            token = Token.objects.get(user=self, token_type=token_type)
            token.token = Token.generate(token_length)
            token.data = data
            token.save()
            return token
        except Token.DoesNotExist:
            return Token.create(self, token_type, data)

    def send_email_with_token(self, token_type, data=''):
        token = self.create_token(token_type, data)
        params = {'email': self.email, 'token': token.token}
        return self.send_email(token_type, params)


# class Account(AbstractAccount):
#     """
#     An instantiated account so
#     """


class Token(models.Model):
    TOKEN_TYPES = {
        'ACTIVATION': {'length': 16},
        'FORGOT_PASSWORD': {'length': 16},
        'CHANGE_EMAIL': {'length': 16}
    }
    user = models.ForeignKey(Account)
    token = models.CharField(max_length=50)
    token_type = models.CharField(max_length=50)
    attempts = models.IntegerField(default=0)
    data = models.CharField(max_length=255, blank=True, default='')

    class Meta:
        unique_together = ('user', 'token_type')

    @staticmethod
    def generate(length=10):
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        unix_time = time.time()
        return hashlib.sha1(salt + str(unix_time)).hexdigest()[:length]

    @staticmethod
    def create(user, token_type, data=''):
        if token_type not in Token.TOKEN_TYPES:
            return None
        token_length = Token.TOKEN_TYPES[token_type]['length']
        token = Token(user=user,
                      token=Token.generate(token_length),
                      token_type=token_type,
                      data=data)
        token.save()
        return token

    @staticmethod
    def get_token_by_type(token, token_type):
        try:
            return Token.objects.get(token_type=token_type, token=token)
        except Token.DoesNotExist:
            return None


class Mail(models.Model):

    MAIL_TYPES = {
        'TEST': {
            'text': 'emails/test.txt',
            'template': 'emails/test.html',
            'subject': 'This is a test email'
        },
        'ACTIVATION': {
            'text': 'emails/activation.txt',
            'template': 'emails/activation.html',
            'subject': 'Welcome! Please activate your account'
        },
        'FORGOT_PASSWORD': {
            'text': 'emails/forgot_password.txt',
            'template': 'emails/forgot_password.html',
            'subject': 'Forgot your password?'
        },
        'CHANGE_EMAIL': {
            'text': 'emails/change_email.txt',
            'template': 'emails/change_email.html',
            'subject': 'Please confirm this email address.'
        },
        'CONFIRM_PASSWORD_CHANGE': {
            'text': 'emails/confirm_password_change.txt',
            'template': 'emails/confirm_password_change.html',
            'subject': 'Your password has been changed.'
        }
    }

    recipient = models.ForeignKey(Account, null=True)
    recipient_email = models.EmailField()
    subject = models.CharField(max_length=255)
    message = models.TextField(blank=True)
    html_message = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    send_counter = models.IntegerField(default=0)
    mail_type = models.CharField(max_length=50)
    access = models.CharField(max_length=50, blank=True)

    def __init__(self, *args, **kwargs):
        if hasattr(settings, 'EMAIL_TEMPLATES'):
            self.MAIL_TYPES.update(settings.EMAIL_TEMPLATES)
        super(Mail, self).__init__(*args, **kwargs)

    @staticmethod
    def get_outbox():
        return mail.outbox

    def send(self, email_addr, mail_type, params):
        if mail_type not in self.MAIL_TYPES:
            err_msg = "%s does not exist in EMAIL_TEMPLATES"\
                      % mail_type
            raise ImproperlyConfigured(err_msg)
        access = Token.generate(16)
        params.update({"BASE_URL": settings.HOSTNAME,
                       'ACCESS_EMAIL': access,
                       'SUPPORT_EMAIL': settings.SUPPORT_EMAIL})
        context = Context(params)
        self.recipient_email = email_addr
        self.mail_type = mail_type
        self.access = access
        self.subject = self.MAIL_TYPES[mail_type]['subject']
        self.message = get_template(
            self.MAIL_TYPES[mail_type]['text']
        ).render(context)
        self.html_message = get_template(
            self.MAIL_TYPES[mail_type]['template']
        ).render(context)
        mail.send_mail(
            self.subject,
            self.message,
            settings.FROM_EMAIL,
            [self.recipient.email],
            False,
            None,
            None,
            None,
            self.html_message
        )
        self.send_counter = 1
        self.save()
        return self

    def send_to_account(self, recipient, mail_type, params):
        self.recipient = recipient
        return self.send(recipient.email, mail_type, params)


