from django.http import JsonResponse


def win(payload, msg='SUCCESS'):

    return JsonResponse({
        'success': True,
        'msg': msg,
        'payload': payload,
        'errors': False
    })


def fail(msg, errors=None, payload=None):

    if not payload:
        payload = {}

    if not errors:
        errors = {}

    return JsonResponse({
        'success': False,
        'msg': msg,
        'payload': payload,
        'errors': errors
    })