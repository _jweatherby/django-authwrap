from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model

from .ajax_responses import win, fail
from .decorators import anonymous_required
from .models import Token, Mail
from .forms import (RegistrationForm,
                    ActivationForm,
                    ForgotPasswordForm,
                    ChangeEmailForm,
                    ChangePasswordForm,
                    LoginForm)


@anonymous_required
def login(request):

    needs_activation = False
    if 'next' in request.GET:
        goto_next = request.GET['next']
    elif 'next' in request.POST:
        goto_next = request.POST['next']
    else:
        goto_next = ''

    if not request.POST:
        return render(request,
                      'authwrap/login.html',
                      {'login_form': LoginForm(),
                       'next': goto_next})

    form = LoginForm(request.POST)
    if form.is_valid():
        user = auth.authenticate(
            email=form.cleaned_data['email'],
            password=form.cleaned_data['password'])
        auth.login(request, user)
        if request.is_ajax():
            return win({'needs_activation': False,
                        'next': goto_next})
        else:
            if goto_next != '':
                return HttpResponseRedirect(goto_next)
            else:
                return HttpResponseRedirect(reverse('home'))

    if 'email' in form.errors \
            and 'not active' in form.errors['email'][0]:
        needs_activation = True

    if request.is_ajax():
        return fail('form_errors',
                    form.errors,
                    {'needs_activation': needs_activation,
                     'next': goto_next})
    else:
        return render(request,
                      'authwrap/login.html',
                      {'login_form': form,
                       'needs_activation': needs_activation,
                       'next': goto_next})


@anonymous_required
def register(request):

    registered = False
    if not request.POST:
        return render(request,
                      'authwrap/register.html',
                      {'registration_form': RegistrationForm(),
                       'registered': registered})

    form = RegistrationForm(request.POST)
    if form.is_valid():
        form.instance.is_active = False
        form.instance.is_admin = False
        acct = form.save()
        acct.assign_password(form.cleaned_data['password1'])
        acct.send_email_with_token('ACTIVATION')
        registered = True
        if request.is_ajax():
            return win({'registered': True})

    if request.is_ajax():
        return fail('form_errors',
                    form.errors,
                    {'registered': registered})
    else:
        return render(request,
                      'authwrap/register.html',
                      {'registration_form': form,
                       'registered': registered})


@anonymous_required
def send_activation(request):

    activation_sent = False
    if not request.POST:
        return render(request,
                      'authwrap/send_activation.html',
                      {'form': ActivationForm()})

    form = ActivationForm(request.POST)
    if form.is_valid():
        account = get_user_model()
        acct = account.objects.get(email=form.cleaned_data['email'])
        acct.send_email_with_token('ACTIVATION')
        activation_sent = True
        if request.is_ajax():
            return win({'activation_sent': True})

    if request.is_ajax():
        return fail('form_errors', form.errors)
    else:
        return render(request,
                      'authwrap/send_activation.html',
                      {'form': form,
                       'reset_sent': activation_sent})


@anonymous_required
def forgot_password(request):

    reset_sent = False
    if not request.POST:
        return render(request,
                      'authwrap/forgot_password.html',
                      {'form': ForgotPasswordForm()})

    form = ForgotPasswordForm(request.POST)
    if form.is_valid():
        account = get_user_model()
        acct = account.objects.get(email=form.cleaned_data['email'])
        acct.send_email_with_token('FORGOT_PASSWORD')
        reset_sent = True
        if request.is_ajax():
            return win({'reset_sent': reset_sent})

    if request.is_ajax():
        return fail('form_errors', form.errors)
    else:
        return render(request,
                      'authwrap/forgot_password.html',
                      {'form': form,
                       'reset_sent': reset_sent})


@login_required
def profile(request):
    return render(request, 'authwrap/profile.html')


@login_required
def change_password(request):

    password_changed = False
    if not request.POST:
        return render(request,
                      'authwrap/change_password.html',
                      {'form': ChangePasswordForm})

    form = ChangePasswordForm(request.POST)
    if form.is_valid():
        acct = request.user
        acct.assign_password(form.cleaned_data['password1'], True)
        auth.update_session_auth_hash(request, acct)
        password_changed = True
        if request.is_ajax():
            return win({'password_changed': password_changed})

    if request.is_ajax():
        return fail('form_errors', form.errors)
    else:
        return render(request,
                      'authwrap/change_password.html',
                      {'form': form,
                       'password_changed': password_changed})


@login_required
def change_email(request):
    email_changed = False
    if not request.POST:
        return render(request,
                      'authwrap/change_email.html',
                      {'form': ChangeEmailForm})

    form = ChangeEmailForm(request.POST)
    if form.is_valid():
        # temporarily change email to send to new address
        email = form.cleaned_data['email']
        acct = get_user_model().objects.get(pk=request.user.pk)
        acct.email = email
        acct.send_email_with_token('CHANGE_EMAIL', email)
        email_changed = True
        if request.is_ajax():
            return win({'email_changed': email_changed})

    if request.is_ajax():
        return fail('form_errors', form.errors)
    else:
        return render(request,
                      'authwrap/change_email.html',
                      {'form': form,
                       'email_changed': email_changed})


@login_required
def deactivate(request):
    if not request.POST:
        return render(request,
                      'authwrap/deactivation.html')

    acct = request.user
    acct.is_active = False
    acct.save()
    auth.logout(request)
    redirect = "%s?msg=DEACTIVATED" % reverse('home')
    return HttpResponseRedirect(redirect)


# backdoor login
def activate(request, token):
    token = get_object_or_404(Token,
                              token=token,
                              token_type='ACTIVATION')

    if request.user \
            and request.user.is_authenticated() \
            and request.user != token.user:
        auth.logout()

    acct = token.user
    acct.is_active = 1
    acct.save()
    token.delete()
    acct.backend = 'django.contrib.auth.backends.ModelBackend'
    auth.login(request, acct)
    return HttpResponseRedirect(reverse('home'))


# backdoor login
def activate_email(request, token):
    token = get_object_or_404(Token,
                              token=token,
                              token_type='CHANGE_EMAIL')

    if request.user \
            and request.user.is_authenticated() \
            and request.user != token.user:
        auth.logout()

    if not request.user.is_authenticated():
        token.user.backend = 'django.contrib.auth.backends.ModelBackend'
        auth.login(request, token.user)

    acct = token.user
    acct.email = token.data
    acct.save()
    token.delete()
    redirect = "%s?msg=EMAIL_ACTIVATED" % reverse('home')
    return HttpResponseRedirect(redirect)


# backdoor login
def reset_password(request, token):
    token = get_object_or_404(Token,
                              token=token,
                              token_type='FORGOT_PASSWORD')

    if request.user \
            and request.user.is_authenticated() \
            and request.user != token.user:
        auth.logout()

    if token.user.is_active is False:
        token.user.is_active = True
        token.user.save()
        Token.objects\
            .filter(user=token.user,
                    token_type='ACTIVATION')\
            .delete()

    token.delete()
    token.user.backend = 'django.contrib.auth.backends.ModelBackend'
    auth.login(request, token.user)
    return HttpResponseRedirect(reverse('authwrap:change-password'))


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('home'))


def view_email(request, access):
    email = get_object_or_404(Mail, access=access)
    return HttpResponse(email.html_message)
