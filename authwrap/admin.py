from django import forms 
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from django.contrib.auth import get_user_model


class AccountChangeForm(UserChangeForm):

    password = forms.CharField(widget=forms.HiddenInput)

    class Meta(UserChangeForm.Meta):
        model = get_user_model()


class AccountCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = get_user_model()

    def clean_email(self):
        email = self.cleaned_data['email']
        model = get_user_model()
        try:
            model.objects.get(email=email)
        except model.DoesNotExist:
            return email
        raise forms.ValidationError(self.error_messages['duplicate_email'])


admin.site.register(get_user_model())