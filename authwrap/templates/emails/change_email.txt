
   Welcome! This is your activation email.

   Please visit {{ BASE_URL }}{% url 'authwrap:activate-email' token=token %} to activate this email address.
   Til then, the old one will remain your current email address.

   If you were not supposed to retrieve this email, let us know.

   {{ SUPPORT_EMAIL }}