from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse

# Create your views here.

def home(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('authwrap:profile'))
    return render(request, 'home.html')